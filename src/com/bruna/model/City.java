package com.bruna.model;

import java.util.ArrayList;

public class City {

	private String name;
	private City prev;
	private City next;
	private Long distance;
	
	public ArrayList<City> getNearCitys(){
		ArrayList<City> near = new ArrayList<City>();
		near.add(this.prev);
		near.add(this.next);
		return near;
	}
	
	public Long getDistance() {
		return distance;
	}
	public void setDistance(Long distance) {
		this.distance = distance;
	}
	public City getPrev() {
		return prev;
	}
	public void setPrev(City prev) {
		this.prev = prev;
	}
	public City getNext() {
		return next;
	}
	public void setNext(City next) {
		this.next = next;
	}
	public City(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
