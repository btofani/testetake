package com.bruna.model;

public class Route {
	
	private Long distance;
	private City origin;
	private City destiny;
	public Long getDistance() {
		return distance;
	}
	public void setDistance(Long distance) {
		this.distance = distance;
	}
	public City getOrigin() {
		return origin;
	}
	public void setOrigin(City origin) {
		this.origin = origin;
	}
	public City getDestiny() {
		return destiny;
	}
	public void setDestiny(City destiny) {
		this.destiny = destiny;
	}
}
