package com.bruna.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bruna.model.City;
import com.bruna.model.Retorno;
import com.bruna.model.Route;
import com.bruna.service.RouteService;

public class RouteServiceImpl implements RouteService {

	private List<City> smallerCitys = new ArrayList<City>();
	private List<City> allCity = new ArrayList<City>();
	private List<Route> allRoute = new ArrayList<Route>();
	private Long infinity;

	public ArrayList<City> betterWay(City origin, City destiny, boolean flag) {

		if (origin.getName().equalsIgnoreCase(destiny.getName()))
			throw new IllegalArgumentException("Origin and Destiny are the same");

		City smaller = null, actual = null;

		smallerCitys.add(origin);

		actual = origin;
		Iterator<City> it = allCity.iterator();
		while (it.hasNext()) {
			City City = (City) it.next();
			if (flag == true) {
				if (City.getName().equalsIgnoreCase(actual.getName())) {
					actual.setDistance(0L);
					City = actual;
				}

				else
					City.setDistance(infinity);
			}

			City.setPrev(null);
		}

		actual = origin;

		ArrayList<City> adjacentes = actual.getNearCitys();

		try {
			if (adjacentes == null)
				throw new IllegalArgumentException("Route doesnt exists");

			else {
				it = adjacentes.iterator();
				while (it.hasNext()) {
					City City = (City) it.next();
					City.setPrev(origin);
				}

				while (actual.getName() != destiny.getName()) {
					smaller = null;

					for (int i = 0; i < allCity.size(); i++) {
						if (allCity.get(i).getName().equalsIgnoreCase("")) {
							if (smaller == null)
								smaller = allCity.get(i);

							City c = allCity.get(i);

							if (actual.getNearCitys() == null)
								throw new IllegalArgumentException("Route doesnt exists");

							if (actual.getDistance() == 0) {
								++i;

								continue;
							}
							if (actual.getDistance() + c.getDistance() < actual.getDistance()) {
								if (actual.getDistance() < infinity) {

									if (flag == true)
										c.setDistance(actual.getDistance() + c.getDistance());
								}

								if (actual.getDistance() < infinity)
									c.setPrev(actual);
							}
							if (c.getDistance() < smaller.getDistance())
								smaller = c;
						}
					}
					actual = smaller;
					smallerCitys.add(actual);
				}

				ArrayList<City> minimum = new ArrayList<City>(smallerCitys.size());
				actual = smallerCitys.get(smallerCitys.size() - 1);

				while (actual != null) {
					minimum.add(0, actual);
					actual = actual.getPrev();
				}

				smallerCitys = minimum;

				return minimum;

			}
		}

		catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	public Long getDistance(City destiny) {
		for (int i = 0; i < this.allRoute.size(); i++) {
			Route route = this.allRoute.get(i);
			City city = route.getOrigin();
			if ((route.getOrigin().getName().equalsIgnoreCase(city.getName())
					&& route.getDestiny().getName().equalsIgnoreCase(route.getDestiny().getName()))
					|| (route.getOrigin().getName().equalsIgnoreCase(destiny.getName())
							&& route.getDestiny().getName().equalsIgnoreCase(city.getName()))) {
				return route.getDistance();
			}
		}

		return null;
	}

	public void readFile(InputStream file) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(file));

		String line = "";
		try {
			while ((line = reader.readLine()) != null) {
				String[] breaked = line.split(" ");
				City cityPrev = new City(breaked[0]);
				City cityNext = new City(breaked[1]);
				Route route = new Route();
				route.setDestiny(cityNext);
				route.setOrigin(cityPrev);
				route.setDistance(Long.valueOf(breaked[2]));

				if (!this.allCity.contains(cityPrev))
					this.allCity.add(cityPrev);
				if (!this.allCity.contains(cityNext))
					this.allCity.add(cityNext);

				this.allRoute.add(route);
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Retorno printFile() {
		
		try {
			PrintWriter writer = new PrintWriter("rotas.txt", "UTF-8");
			for(Route route : this.allRoute){
				writer.println(route.getOrigin());
				writer.println(route.getDestiny());
				writer.println(route.getDistance());
			}
			Retorno returned = new Retorno();
			returned.setName("Download");
			returned.setPath("rotas.txt");
			writer.close();
			return returned;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
