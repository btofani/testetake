package com.bruna.service;

import java.io.InputStream;
import java.util.ArrayList;

import com.bruna.model.City;
import com.bruna.model.Retorno;

public interface RouteService {
	
	public ArrayList<City> betterWay(City origin, City destiny, boolean flag);
	
	public Long getDistance(City destiny);
	
	public void readFile(InputStream file);
	
	public Retorno printFile();
	
}
