package com.bruna;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.bruna.model.Retorno;
import com.bruna.service.RouteService;
import com.sun.jersey.multipart.FormDataParam;
 
@Path("/routeController")
public class RouteController {
	
	@Autowired
	RouteService routeService ;
	
	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Retorno uploadFile(@FormDataParam("file") InputStream uploadedInputStream) {
		
		routeService.readFile(uploadedInputStream);
		return routeService.printFile();
	}
	
//	@GET
//	@Path("/getFiles")
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<Route> getFiles() {
//		return routeService.getFiles();
//	}

}
