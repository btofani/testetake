root.controller('uploadController', function($scope, $http, fileUploadService) {

	$scope.fileList = [];
	
	$scope.setFile = function($files) {
		
		var objFile = {};
		objFile.file = $files[0];
		objFile.horaEnvio = objFile.file.lastModifiedDate;
		objFile.name = objFile.file.name;
		objFile.status = "Aguardando Envio";
		$scope.file = objFile;
		
		$scope.addFile($scope.file);
	};
	
	$scope.uploadFile = function() {
		
		angular.forEach($scope.fileList, function(value, key) {
			
			if(value.status != "Enviado"){
				promise = fileUploadService.uploadFile(value.file, value.name);
				promise.then(function(response) {
					value = response;
					value.status = "Enviado";
				}, function() {
					value.status = "Erro ao Enviar";
				})
			}
			console.log(value);
		});		
	}
	
	$scope.getFiles = function() {
		
		promise = fileUploadService.getFiles();

		promise.then(function(response) {
			$scope.fileList = response;
		}, function() {
			$scope.serverResponse = 'Erro';
		})
	}
	
	$scope.addFile = function(file){
		
		$scope.fileList.push(file);
		console.log($scope.fileList);
		$scope.$apply();
	}
});